jQuery(document).ready(function () {
  jQuery('#auth').submit(function(e) {
    e.preventDefault();
    var data = {
      login: jQuery('[id = "auth-email login"]').val(),
      pass: jQuery('[id = "auth-password Password"]').val()
    };
    sendPost(data);
  });

  jQuery('#delcookie').click(function(){
    sendPost({delcookie: "delcookie"});
  });

});

function sendPost(params) {
  jQuery.ajax({
    type: 'POST',
    url: '/',
    data: params,
    success: function(data, status, resp) {
      location.reload();
    }
  })
}