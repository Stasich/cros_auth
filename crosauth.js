jQuery(document).ready(function () {
  if (window.addEventListener) {
    window.addEventListener("message", listener);
  } else {
    // IE8
    window.attachEvent("onmessage", listener);
  }

  if (document.domain === 'cookie1.com.ua') {
    var iframes = '<iframe name = "cookie1.stasich" src="http://cookie1.stasich" style="display:none">' +
      '</iframe>'+
      '<br>'+
      '<iframe name = "'+document.domain+'" src="http://'+document.domain +'" style="display:none">' +
      '</iframe>';
    jQuery('body').append(iframes);

    jQuery('#auth').submit(function() {
      var logo_pass = {
        login: jQuery('[id = "auth-email login"]').val(),
        pass: jQuery('[id = "auth-password Password"]').val()
      };
      var win1 = window.frames['cookie1.stasich'];
      var win2 = window.frames[document.domain];
      win1.postMessage(logo_pass, 'http://cookie1.stasich');
      win2.postMessage(logo_pass, 'http://'+document.domain);

    });
  }
});

function listener(event) {
  if (event.origin !== 'http://cookie1.com.ua') {
    return;
  }

  jQuery.ajax({
    type: 'POST',
    url: '/cros_auth.php',
    data: event.data,
    success: function(data, status, resp) {
        if (document.domain !== 'cookie1.stasich')
          event.source.location.reload();
    }
  });
}